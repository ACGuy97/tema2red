package com.barranquero.tema2red;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import java.io.File;
import java.io.FileNotFoundException;

import cz.msebera.android.httpclient.Header;

/**
 * Created by usuario on 17/11/16.
 */
public class Subida extends AppCompatActivity {
    public final static String WEB = "http://bitbits.hopto.org/ACDAT/upload.php";
    EditText edtArchivo;
    TextView txvInfo;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subida);
        setTitle("Subida de archivos");

        edtArchivo = (EditText)findViewById(R.id.edtArchivo);
        txvInfo = (TextView)findViewById(R.id.txvInfo);
    }

    public void onClickSubida(View v) {
        subida();
    }

    private void subida() {
        String fichero = edtArchivo.getText().toString();
        final ProgressDialog progreso = new ProgressDialog(Subida.this);
        File myFile;
        Boolean existe = true;
        myFile = new File(Environment.getExternalStorageDirectory(), fichero);
//File myFile = new File("/path/to/file.png");
        RequestParams params = new RequestParams();
        try {
            params.put("fileToUpload", myFile);
            params.put("password", "1234");
        } catch (FileNotFoundException e) {
            existe = false;
            txvInfo.setText("Error en el fichero: " + e.getMessage());
//Toast.makeText(this, "Error en el fichero: " + e.getMessage(), Toast.LENGTH_SHORT).show();
        }
        if (existe)
            RestClient.post(WEB, params, new TextHttpResponseHandler() {
                @Override
                public void onStart() {
// called before request is started
                    progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progreso.setMessage("Conectando . . .");
                    progreso.setCancelable(false);
                    progreso.setOnCancelListener(new DialogInterface.OnCancelListener() {
                        public void onCancel(DialogInterface dialog) {
                            RestClient.cancelRequests(getApplicationContext(), true);
                        }
                    });
                    progreso.show();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, String response) {
// called when response HTTP status is "200 OK"
                    progreso.dismiss();
                    txvInfo.setText(response);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, String response, Throwable t) {
// called when response HTTP status is "4XX" (eg. 401, 403, 404)
                    progreso.dismiss();
                    txvInfo.setText("Fallo: "+ statusCode+ "\n" + response + "\n");
                }
            });
    }
}
