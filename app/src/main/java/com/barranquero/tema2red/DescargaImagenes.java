package com.barranquero.tema2red;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.FileAsyncHttpResponseHandler;
import com.squareup.picasso.Picasso;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;

import cz.msebera.android.httpclient.Header;

/**
 * Created by les gold from the 8miles
 */
public class DescargaImagenes extends AppCompatActivity implements View.OnClickListener {
    EditText texto;
    Button botonImagen, botonFichero;
    ImageView imagen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_descarga_imagenes);
        setTitle("Descarga");
//http://i.imgur.com/hlWzRAQ.jpg
        texto = (EditText) findViewById(R.id.editText);
        botonImagen = (Button) findViewById(R.id.button);
        botonImagen.setOnClickListener(this);
        botonFichero = (Button)findViewById(R.id.button2);
        botonFichero.setOnClickListener(this);
        imagen = (ImageView) findViewById(R.id.imageView);
    }
    @Override
    public void onClick(View v) {
        String url;
        if (v == botonImagen) {
            url = texto.getText().toString();
            Picasso.with(getApplicationContext())
                    .load(url)
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.error)
                    .resize(300, 400)
                    .rotate(45)
                    .into(imagen);
        }
        if (v == botonFichero) {
            url = texto.getText().toString();
            AAHC(url);
        }
    }

    private void AAHC(String url) {
        final ProgressDialog progreso = new ProgressDialog(this);
        File miFichero = new File(Environment.getExternalStorageDirectory().getAbsolutePath(), "descarga");

        RestClient.get(url, new FileAsyncHttpResponseHandler(miFichero) {
            @Override
            public void onStart() {
                super.onStart();
                progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progreso.setMessage("Conectando . . .");
                progreso.setCancelable(false);
                progreso.show();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {
                progreso.dismiss();
                Toast.makeText(getApplicationContext(), "Fallo: " + throwable.getMessage(), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, File file) {
                progreso.dismiss();
                Toast.makeText(getApplicationContext(), "Descarga OK\n " + file.getPath(), Toast.LENGTH_LONG).show();
            }
        });
    }
}
