package com.barranquero.tema2red;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class Sidoso extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sidoso);
    }

    public void onClickMenu(View view) {
        Intent intent;
        switch (view.getId()) {
            case R.id.btnConexHTTP:
                intent = new Intent(Sidoso.this, ConexHTTP.class);
                startActivity(intent);
                break;
            case R.id.btnConexAsin:
                intent = new Intent(Sidoso.this, ConexAsincrona.class);
                startActivity(intent);
                break;
            case R.id.btnVolley:
                intent = new Intent(Sidoso.this, ConexVolley.class);
                startActivity(intent);
                break;
            case R.id.btnDescarga:
                intent = new Intent(Sidoso.this, DescargaImagenes.class);
                startActivity(intent);
                break;
            case R.id.btnSubida:
                intent = new Intent(Sidoso.this, Subida.class);
                startActivity(intent);
                break;
        }
    }
}
