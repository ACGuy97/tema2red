package com.barranquero.tema2red;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.ResolveInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.os.SystemClock;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import com.loopj.android.http.TextHttpResponseHandler;

/**
 * Created by usuario on 7/11/16.
 */

public class ConexAsincrona extends AppCompatActivity {
    EditText direccion;
    RadioButton radioJava, radioApache, radioAAHC;
    Button conectar;
    WebView web;
    TextView tiempo;
    public static final String JAVA = "Java";
    public static final String APACHE = "Apache";
    long inicio, fin;
    TareaAsincrona miMierda;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conex_asincrona);

        direccion = (EditText) findViewById(R.id.direccion);
        radioJava = (RadioButton) findViewById(R.id.radioJava);
        radioApache = (RadioButton) findViewById(R.id.radioApache);
        radioAAHC = (RadioButton) findViewById(R.id.radioAAHC);
        conectar = (Button) findViewById(R.id.conectar);

        web = (WebView) findViewById(R.id.web);
        tiempo = (TextView) findViewById(R.id.resultado);
        //StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().permitNetwork().build());
    }

    public void onClickAsinc(View v) {
        String texto = direccion.getText().toString();
        String tipo = APACHE;

        Resultado resultado;
        if (v == conectar) {
            inicio = System.currentTimeMillis();

            if (radioAAHC.isChecked())
                AAHC();
            else if (radioJava.isChecked()) {
                tipo = JAVA;
                miMierda = new TareaAsincrona(this);
                miMierda.execute(tipo, texto);
                tiempo.setText("ESSSSPERANDO...");
            }
        }
    }


    public class TareaAsincrona extends AsyncTask<String, Integer, Resultado> {
        private ProgressDialog progreso;
        private Context context;

        public TareaAsincrona(Context context) {
            this.context = context;
        }

        protected void onPreExecute() {
            progreso = new ProgressDialog(context);
            progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progreso.setMessage("Conectando . . .");
            progreso.setCancelable(true);
            progreso.setOnCancelListener(new DialogInterface.OnCancelListener() {
                public void onCancel(DialogInterface dialog) {
                    TareaAsincrona.this.cancel(true);
                }
            });
            progreso.show();
        }

        protected Resultado doInBackground(String... cadena) {
            Resultado resultado;
            int i = 1;
            try {
                // operaciones en el hilo secundario
                publishProgress(i++);
                if (cadena[0].equals(JAVA))
                    resultado = Conexion.conectarJava(cadena[1]);
                else
                    resultado = Conexion.conectarApache(cadena[1]);
            } catch (Exception e) {
                Log.e("Sidoso: ", e.getLocalizedMessage());
                resultado = new Resultado();
                resultado.setCodigo(false);
                resultado.setContenido(e.getMessage());
                //cancel(true);
            }
            return resultado;
        }

        protected void onProgressUpdate(Integer... progress) {
            progreso.setMessage("Conectando " + Integer.toString(progress[0]));
        }

        protected void onPostExecute(Resultado result) {
            progreso.dismiss();
            fin = System.currentTimeMillis();
            // mostrar el resultado
            if (result.isCodigo())
                web.loadDataWithBaseURL(null, result.getContenido(), "text/html", "UTF-8", null);
            else
                web.loadDataWithBaseURL(null, result.getMensaje(), "text/html", "UTF-8", null);
            tiempo.setText("Duración: " + String.valueOf(fin - inicio) + " milisegundos");
        }

        protected void onCancelled() {
            progreso.dismiss();
            fin = System.currentTimeMillis();
            // mostrar cancelación
            web.loadDataWithBaseURL(null, "Cancerado", "text/html", "UTF-8", null);
            tiempo.setText("Duración: " + String.valueOf(fin - inicio) + " milisegundos");
        }
    }

    private void AAHC() {
        final String texto = direccion.getText().toString();
        final long inicio;
        final long[] fin = new long[1];
        final ProgressDialog progreso = new ProgressDialog(ConexAsincrona.this);
        inicio = System.currentTimeMillis();
        RestClient.get(texto, new TextHttpResponseHandler() {
            @Override
            public void onStart() {
                // called before request is started
                progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progreso.setMessage("Conectando . . .");
                //progreso.setCancelable(false);
                progreso.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    public void onCancel(DialogInterface dialog) {
                        RestClient.cancelRequests(getApplicationContext(), true);
                    }
                });
                progreso.show();
            }

            @Override
            public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, String responseString, Throwable throwable) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                fin[0] = System.currentTimeMillis();
                progreso.dismiss();
                web.loadDataWithBaseURL(null, "fallo: "+responseString, "text/html", "UTF-8", null);
                tiempo.setText("Duración: " + String.valueOf(fin[0] - inicio) + " milisegundos");
            }

            @Override
            public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, String responseString) {
                // called when response HTTP status is "200"
                fin[0] = System.currentTimeMillis();
                progreso.dismiss();
                web.loadDataWithBaseURL(null, responseString, "text/html", "UTF-8", null);
                tiempo.setText("Duración: " + String.valueOf(fin[0] - inicio) + " milisegundos");
            }
        });

    }
}
